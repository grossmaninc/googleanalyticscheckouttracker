<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'GrossmanInteractive_GoogleAnalyticsCheckoutTracker',
    __DIR__
);
