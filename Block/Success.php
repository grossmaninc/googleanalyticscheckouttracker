<?php

namespace GrossmanInteractive\GoogleAnalyticsCheckoutTracker\Block;

class Success extends \Magento\Framework\View\Element\Template
{
    public function __construct(\Magento\Catalog\Block\Product\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
}
